# (Winry) Bash Helpers

Helper functions primarily used for my own `winry` bash scripts, but can be freely be `source`'d (imported) from any bash script.

I prefer a very verbose and fluent language approach to programming and naming for functions and variables. Hopefully this will help anybody reading the code to understand exactly what is going on with out/minimal comments.

## Get Started
Clone this repository to your local/target machine. You then have two choices on you `source` (import) the helpers

### Source helpers directly from repository clone
You source the required helpers in your script directly from the repository clone

> Please be aware this has the limitation that the script will always need this repo present in order to function.

### Copy the helpers to your projects folder
This is the recommended method to source helpers into your scripts.

```bash
$ cd /to/local/repository/clone
$ ./make.sh for-cli <path/to/project> [options]
# ...
$ [Success]    Stub copied to /path/to/project/winry/winry_foundation
$ [Success]    Helpers written to /path/to/project/winry/winry_foundation
```

You can then source them using one of the following methods

```bash
# ...
\. '/local/path/to/helpers' # I find this method is more portable, but that just me

# Alternatively you can use
source '/local/path/to/helpers'
# ...
```

## Constants
All constants are exported as readonly.

### Error Codes
These constants represent error/exit codes.

| Constant | Exit Code  |
|----------|------------|
| WINRY_E_INVALID_ARGUMENTS_COUNT  | 10
| WINRY_E_INVALID_ARGUMENTS        | 11
| WINRY_E_UNDEFINED_VALUE          | 13
| WINRY_E_ALREADY_EXISTS           | 14
| WINRY_E_WRITE_FAILED             | 15
| WINRY_E_SYSTEM_UNSUPPORTED       | 99
| WINRY_E_COPY_FAILED              | 20
| WINRY_E_MOVE_FAILED              | 21
| WINRY_E_CHANGE_PERMISSION_FAILED | 30
| WINRY_E_NOT_FOUND                | 40
| WINRY_E_COMMAND_NOT_FOUND        | 41
| WINRY_E_PROGRAM_NOT_INSTALLED    | 42

## Functions

`winry_assert_*` functions exit with one of the error constants listed above.

### Foundation [./src/foundation]

| Name |
|------|
| winry_function_called_with_arguments
| winry_function_called_with_no_arguments
| winry_assert_function_called_with_arguments
| winry_function_called_with_two_arguments
| winry_assert_function_called_with_two_arguments
| winry_function_called_with_three_arguments
| winry_assert_function_called_with_three_arguments
| winry_param_exists
| winry_param_is_set
| winry_param_is_not_set
| winry_assert_param_is_set
| winry_param_is_empty
| winry_param_is_not_empty
| winry_assert_param_is_empty
| winry_assert_param_is_not_empty
| winry_greater_than
| winry_function_exists
| winry_function_does_not_exists
| winry_file_exists
| winry_file_does_not_exist
| winry_assert_file_exists
| winry_assert_file_does_not_exist
| winry_path_exists
| winry_path_does_not_exist
| winry_assert_path_exists
| winry_assert_path_does_not_exists
| winry_create_path
| winry_path_created
| winry_assert_create_path
| winry_assert_path_created
| winry_copy_path
| winry_copy_path_failed
| winry_assert_path_copied
| winry_assert_file_copied
| winry_create_backup_file
| winry_delete_file
| winry_symlink_file
| winry_symlink_directory
| winry_running_on_a_battery
| winry_is_not_running_on_a_battery
| winry_print
| winry_print_red
| winry_print_green
| winry_print_yellow
| winry_print_bold_yellow
| winry_print_blue
| winry_print_magenta
| winry_print_cyan
| winry_print_bold
| winry_print_error
| winry_print_success
| winry_print_warning
| winry_print_info
| winry_print_debug
| winry_print_line
| winry_print_yellow_line
| winry_print_error_message
| winry_running_as_sudo
| winry_assert_running_as_sudo
| winry_program_is_installed
| winry_program_is_not_installed
| winry_assert_program_is_installed

### Commands Helpers [./src/command]

| Name |
| ---- |
| winry_parse_base_options
| winry_bootstrap
| winry_export_verbose_level
| winry_show_verbose_level_to_name
| winry_print_command
| winry_usage_indentation
| winry_usage_it_gap
| winry_show_verbose_level
| winry_show_version
| winry_show_version_number
| winry_print_usage_line
| winry_print_command_usage_line
| winry_print_base_usage


### String Helpers [./src/string]

| Name |
|------|
| winry_left_trim
| winry_right_trim
| winry_string_to_lower
| winry_string_to_uppercase
| winry_split_title_case
| winry_upper_case_first_character

### OS Helpers [./src/os]

| Name |
|------|
| winry_resolve_system_info
| winry_export_os_opener
| winry_export_sed_command

### Path Helpers [./src/path]

| Name |
|------|
| winryc_base_path

### Self Doc Helpers [./src/self_doc]

| Name |
|------|
| winry_print_self_doc_value
