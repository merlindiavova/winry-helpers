#!/bin/bash

set -Eeuo pipefail

# Name:        Winry Helpers Make
# Version:     0.1.0
# Signature:   ./make.sh <options> [--] <command>
# Description: Correctly copy and amalgamate helpers to project folders
# Maintainer:  Merlin Diavova <merlin@amesplash.co.uk>
# Author:      Merlin Diavova

winry_show_usage() {
  BASE_TAB_LENGTH=25
  export readonly BASE_TAB_LENGTH

  local -r signature="$(winry_print_self_doc_value "Signature")"

  winry_show_version

  winry_print_bold_yellow 'Usage'
  winry_print "  $signature"
  command printf '\n'
  winry_print_bold_yellow "Options:"

  winry_print_usage_line '-h, --help' 'Display this help message'
  winry_print_usage_line '-q, --quiet' 'Do not output any message'
  winry_print_usage_line '-V, --version' 'Display this application version'
  winry_print_usage_line '-v|vv|vvv, --verbose' 'Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug'
  command printf '\n'

  winry_print_bold_yellow "Available commands:"
  winry_print_usage_line 'for:cli' 'Creates a foundation file for use with CLI command/applications'
}

winry_for_cli() {
  local with_usage='no'
  local with_parse_arguments='no'
  local command_file=''

  declare -a command_arguments=()

  declare -a file_list=(\
    './src/os' \
    './src/string' \
    './src/self_doc' \
    './src/command'
  )

  while [[ "${1+defined}" ]]; do
    case "$1" in
      -u|--with-usage) shift 1; with_usage='yes' ;;
      -p|--with-parse-arguments) shift 1; with_parse_arguments='yes' ;;
      -c|--command-file) shift 1; command_file="${1}" ;;

      *) command_arguments+=("$1") ;;
    esac
    shift
  done

  winry_assert_file_exists "${command_arguments[0]}";

  readonly with_usage with_parse_arguments

  if [[ "${with_usage}" == 'yes' ]];
  then
    file_list+=('./src/base_usage_command')
  fi

  if [[ "${with_parse_arguments}" == 'yes' ]];
  then
    file_list+=('./src/base_parse_arguments_command')
  fi

  for file in "${file_list[@]}";
  do
    winry_assert_file_exists "${file}";
  done

  local -r path="${command_arguments[0]}"
  local -r stub_file='winry_foundation'
  local -r foundation_destination="${path}/winry/${stub_file}"
  local -r stub_path='./src/foundation'

  if winry_path_does_not_exist "${path}/winry";
  then
    winry_assert_path_created "${path}/winry"
  fi

  copy_foundation_file "${stub_path}" "${foundation_destination}"
  write_to_foundation_file "${foundation_destination}" "${file_list[@]}"

  if winry_param_is_not_empty "$command_file";
  then
    local -r command_destination="${path}/${command_file}"
    write_command_file "${command_destination}"
  fi

}

copy_foundation_file() {
  winry_assert_function_called_with_two_arguments "$@"

  local -r stub_file_path="${1}"
  local -r foundation_file_path="${2}"

  winry_delete_file "${foundation_file_path}"

  winry_assert_file_copied \
      "${stub_file_path}" \
      "${foundation_file_path}"

  winry_print_success "Stub copied to ${foundation_file_path}"
}

write_to_foundation_file() {
  winry_assert_function_called_with_arguments "$@"

  local -r foundation_file_path="${1}"
  shift

  for file in "${@}";
  do
    winry_assert_file_exists "${file}";

    {
      command printf "\n";
      tail -n +3 "$file";
    } >> "$foundation_file_path"
  done

  winry_print_success "Helpers written to ${foundation_file_path}"
}

write_command_file() {
  local -r command_file_path="$1"
  local -r base_segment='./src/command_base'

  declare -a segments=(\
    './src/base_usage_command'
    './src/base_parse_arguments_command'
    './src/command_main_base'
  )

  if winry_file_exists "$command_file_path";
  then
    winry_print_yellow "Cannot overwrite ${command_file_path}"
  else
    cat "${base_segment}" >> "$command_file_path"

    for file in "${segments[@]}";
    do
      {
        command printf "\n";
        tail -n +3 "$file";
      } >> "$command_file_path"
    done

    winry_print_success "Created command file ${command_file_path}"
  fi
}

winry_parse_arguments() {
  winry_function_called_with_no_arguments "$@" && winry_show_usage && exit

  declare -a command_arguments=()

  while [[ "${1+defined}" ]]; do
    case "$1" in
      -h|--help) winry_show_usage; return ;;
      -V|--version) winry_show_version; return ;;

      for:cli) shift 1; winry_for_cli "$@"; return ;;

      # append to command arguments
      *) command_arguments+=("$1") ;;
    esac
    shift
  done
}

# shellcheck disable=SC1091
main() {
  \. ./src/foundation
  \. ./src/os
  \. ./src/string
  \. ./src/self_doc
  \. ./src/command

  winry_bootstrap "$@"
  winry_parse_arguments "${WINRY_COMMAND[@]}"
}

main "$@"
