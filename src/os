#!/bin/bash

winry_resolve_system_info() {
  local -r kernel_name="$(uname -s)"

  case "${kernel_name}" in
    Linux*)             WINRY_SYSTEM_MACHINE='linux' ;;
    Darwin*)            WINRY_SYSTEM_MACHINE='mac' ;;
    MINGW64_NT-10.0*)   WINRY_SYSTEM_MACHINE='mingw64' ;;
    *)                  WINRY_SYSTEM_MACHINE='UNKNOWN' ;;
  esac

  if [ "$WINRY_SYSTEM_MACHINE" == 'UNKNOWN' ]; then
    winry_print_error_message "${WINRYC_E_SYSTEM_UNSUPPORTED}" \
    && exit "${WINRY_E_SYSTEM_UNSUPPORTED}"
  fi

  winry_print_debug "Running on ${WINRY_SYSTEM_MACHINE} system"
  export readonly WINRY_SYSTEM_MACHINE
}

winry_export_os_opener() {
  # Check for various OS openers. Quit as soon as we find one that works.
  for opener in browser-exec xdg-open cmd.exe cygstart "start" open;
  do
    if command -v $opener >/dev/null 2>&1;
    then
      if [[ "$opener" == "cmd.exe" ]];
      then
        OPEN_BROWSER_COMMAND="$opener /c start";
      else
        OPEN_BROWSER_COMMAND="$opener";
      fi

      export readonly OPEN_BROWSER_COMMAND
      winry_print_debug "Using open command: $OPEN_BROWSER_COMMAND"
      break;
    fi
  done
}

winry_export_sed_command() {
  WINRY_SED_COMMAND='sed -i'
  if [ "$WINRY_SYSTEM_MACHINE" == "mac" ]; then
    WINRY_SED_COMMAND='sed -i .bak'
  fi

  export readonly WINRY_SED_COMMAND

  winry_print_debug "Using sed command: $WINRY_SED_COMMAND"
}
